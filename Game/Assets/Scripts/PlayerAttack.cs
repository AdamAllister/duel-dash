﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(DealDamage))]

public class PlayerAttack : MonoBehaviour {
	private DealDamage dealDamage;
	private string Attacktype;

	private List<KeyValuePair<string, GameObject>> colliderEnemies = new List<KeyValuePair<string, GameObject>>();//List<KeyValuePair<string, GameObject>>

	void Awake()
	{
		dealDamage = GetComponent<DealDamage> ();
	}

	void Update()
	{
		List<GameObject> removalList = new List<GameObject> ();
		if (colliderEnemies.Count > 0) {
			Attacktype = GetComponent<AttackBehaviours>().attackType;
			foreach(KeyValuePair<string, GameObject> colliderEnemy in colliderEnemies) 
			{
				if (Attacktype == colliderEnemy.Key && !colliderEnemy.Value.GetComponent<Health>().flashing && !this.GetComponent<Health>().flashing) 
				{
					int bslevel = 0;
					if (Attacktype == "GBuckler" || Attacktype == "ABuckler")
						bslevel = GetComponent<AttackBehaviours>().bucklerLevel;
					if (Attacktype == "GScimitar" || Attacktype == "AScimitar")
						bslevel = GetComponent<AttackBehaviours>().scimitarLevel;
					if (bslevel == 0)
						dealDamage.Attack (colliderEnemy.Value, 1, 0, 0);
					else if (Attacktype == "GBuckler" || Attacktype == "ABuckler")
					{
						if (bslevel < 3)
							dealDamage.Attack (colliderEnemy.Value, 2, 0, 0);
						else if (bslevel < 4)
							dealDamage.Attack (colliderEnemy.Value, 2, 4, 40);
						else
							dealDamage.Attack (colliderEnemy.Value, 3, 4, 40);
					}
					else if (Attacktype == "GScimitar" || Attacktype == "AScimitar")
					{
						if (bslevel < 4)
							dealDamage.Attack (colliderEnemy.Value, 2, 0, 0);
						else
							dealDamage.Attack (colliderEnemy.Value, 3, 0, 0);
					}
					if (colliderEnemy.Value.GetComponent<Health>().currentHealth < 1)
					{
						if (Attacktype == "GBuckler" || Attacktype == "ABuckler")
						{
							GetComponent<AttackBehaviours>().bucklerXP += 1;
						}
						if (Attacktype == "GScimitar" || Attacktype == "AScimitar")
						{
							GetComponent<AttackBehaviours>().scimitarXP += 1;
						}
					}
				}
				if (!removalList.Contains(colliderEnemy.Value) && colliderEnemy.Value.GetComponent<Health>().currentHealth <= 0) {
					removalList.Add(colliderEnemy.Value);
				}
			}
			for(int i = colliderEnemies.Count - 1; i >= 0; i--) {
				foreach (GameObject removalEnemy in removalList) {
					if (colliderEnemies[i].Value.Equals(removalEnemy)) {
						colliderEnemies.Remove(colliderEnemies[i]);
						break;
					}
				}

			}
		}
	}

	public void addEnemy(GameObject enemy, string key){
		colliderEnemies.Add (new KeyValuePair<string, GameObject> (key, enemy));
	}

	public void removeEnemy(GameObject enemy, string key){
		KeyValuePair<string, GameObject> colliderEnemy = new KeyValuePair<string, GameObject> (key, enemy);
		if(colliderEnemies.Contains(colliderEnemy))
		   colliderEnemies.Remove(colliderEnemy);
	}
}
