﻿using UnityEngine;
using System.Collections;

public class ArrowAttack : MonoBehaviour {

	public string[] tagsToCheck;
	public GameObject player;

	void Awake()
	{
		if(!GetComponent<Collider>() || (GetComponent<Collider>() && !GetComponent<Collider>().isTrigger))
			Debug.LogError ("'ArrowAttack' script attached to object which does not have a trigger collider", transform);
	}
	
	//see if anything entered trigger, filer by tag, store the object
	void OnTriggerEnter (Collider other)
	{
		if (tagsToCheck.Length > 0)
		{
			foreach (string tag in tagsToCheck)
			{
				if (other.tag == tag  && other.transform.gameObject != player) 
				{
					if(other.tag == "Player" && other.gameObject.GetComponent<AttackBehaviours>().bucklerLevel == 5 && other.gameObject.GetComponent<AttackBehaviours>().attackType == "GBuckler"){
						player = other.gameObject;
						GetComponent<Rigidbody>().velocity *= -1; 
					}
					else{
						if (player.GetComponent<AttackBehaviours>().crossbowLevel < 4)
							player.GetComponent<DealDamage>().Attack(other.gameObject, 1, 0, 0);
						else
							player.GetComponent<DealDamage>().Attack(other.gameObject, 2, 0, 0);
						if (other.gameObject.GetComponent<Health>().currentHealth < 1)
						{
							player.GetComponent<AttackBehaviours>().crossbowXP += 1;
						}
						break;
					}
				}
				
			}
		}
		if (other.tag != transform.tag && (other.transform.parent == null || other.transform.parent.tag != player.tag)) {

			if (other.gameObject != player) {
				if (player.GetComponent<AttackBehaviours> ().crossbowLevel < 3) {
					Destroy (this.gameObject);
				} else {
					if (other.tag != "Enemy")
						Destroy (this.gameObject);
				}
			}
		}
	}
}
