﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

	public Arenascript arena;

	public void loadGame(){

		hideMenu ();
		arena.arenaState = "start";
		arena.timer = 5f;
	}

	public void loadFirstGame(){
		
		hideMenu ();
		arena.arenaState = "tutorial";
		arena.timer = 22.75f;
	}

	public void showMenu(){
		foreach (Transform tf in transform) {
			tf.gameObject.SetActive(true);
		}
		gameObject.SetActive (true);

//		foreach (Transform tf in arena.progress.transform) {
//			tf.gameObject.SetActive(false);
//		}
		arena.progress.SetActive (false);
	}

	private void hideMenu(){
		foreach (Transform tf in transform) {
			tf.gameObject.SetActive(false);
		}
		gameObject.SetActive (false);

//		foreach (Transform tf in arena.progress.transform) {
//			tf.gameObject.SetActive(true);
//		}
	}

	public void quit() {
		Debug.Log ("QUIT");
		Application.Quit ();
	}
}
