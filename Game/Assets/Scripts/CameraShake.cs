﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	private Transform cameraTransform;
	public float shake = 0f;
	public float shakeAmt = 0.5f;
	public float decreateFactor = 1.0f;

	// Use this for initialization
	void Start () {
		cameraTransform = transform; 
	}
	
	void Update() {
		if (shake > 0f) {
			//camera.transform.position = Random.insideUnitSphere * shakeAmt
			cameraTransform.transform.localPosition += Random.insideUnitSphere * shakeAmt;
			shake -= Time.deltaTime * decreateFactor;
		} else {
			shake = 0.0f;
		}
	}
}
